import Mock from 'mockjs2'
import { builder } from '../util'

const info = options => {
  console.log('options', options)
  const userInfo = {
    id: '4291d7da9005377ec9aec4a71ea837f',
    name: '天野远子',
    username: 'admin',
    password: '',
    avatar: '/avatar2.jpg',
    status: 1,
    telephone: '',
    lastLoginIp: '27.154.74.117',
    lastLoginTime: 1534837621348,
    creatorId: 'admin',
    createTime: 1497160610259,
    merchantCode: 'TLif2btpzg079h15bk',
    deleted: 0,
    roleId: 'admin',
    role: {}
  }
  // role
  const roleObj = {
    id: 'admin',
    name: '管理员',
    describe: '拥有所有权限',
    status: 1,
    creatorId: 'system',
    createTime: 1497160610259,
    deleted: 0,
    permissions: [
      {
        roleId: 'admin',
        permissionId: 'dashboard',
        permissionName: '仪表盘',
        actions:
          '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'exception',
        permissionName: '异常页面权限',
        actions:
          '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'result',
        permissionName: '结果权限',
        actions:
          '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'profile',
        permissionName: '详细页权限',
        actions:
          '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'table',
        permissionName: '表格权限',
        actions:
          '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"import","defaultCheck":false,"describe":"导入"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"}]',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'import',
            describe: '导入',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'form',
        permissionName: '表单权限',
        actions:
          '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'order',
        permissionName: '订单管理',
        actions:
          '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'permission',
        permissionName: '权限管理',
        actions:
          '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'role',
        permissionName: '角色管理',
        actions:
          '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'table',
        permissionName: '桌子管理',
        actions:
          '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"query","defaultCheck":false,"describe":"查询"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"}]',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'query',
            describe: '查询',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      },
      {
        roleId: 'admin',
        permissionId: 'user',
        permissionName: '用户管理',
        actions:
          '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"import","defaultCheck":false,"describe":"导入"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"},{"action":"export","defaultCheck":false,"describe":"导出"}]',
        actionEntitySet: [
          {
            action: 'add',
            describe: '新增',
            defaultCheck: false
          },
          {
            action: 'import',
            describe: '导入',
            defaultCheck: false
          },
          {
            action: 'get',
            describe: '详情',
            defaultCheck: false
          },
          {
            action: 'update',
            describe: '修改',
            defaultCheck: false
          },
          {
            action: 'delete',
            describe: '删除',
            defaultCheck: false
          },
          {
            action: 'export',
            describe: '导出',
            defaultCheck: false
          }
        ],
        actionList: null,
        dataAccess: null
      }
    ]
  }

  roleObj.permissions.push({
    roleId: 'admin',
    permissionId: 'support',
    permissionName: '超级模块',
    actions:
      '[{"action":"add","defaultCheck":false,"describe":"新增"},{"action":"import","defaultCheck":false,"describe":"导入"},{"action":"get","defaultCheck":false,"describe":"详情"},{"action":"update","defaultCheck":false,"describe":"修改"},{"action":"delete","defaultCheck":false,"describe":"删除"},{"action":"export","defaultCheck":false,"describe":"导出"}]',
    actionEntitySet: [
      {
        action: 'add',
        describe: '新增',
        defaultCheck: false
      },
      {
        action: 'import',
        describe: '导入',
        defaultCheck: false
      },
      {
        action: 'get',
        describe: '详情',
        defaultCheck: false
      },
      {
        action: 'update',
        describe: '修改',
        defaultCheck: false
      },
      {
        action: 'delete',
        describe: '删除',
        defaultCheck: false
      },
      {
        action: 'export',
        describe: '导出',
        defaultCheck: false
      }
    ],
    actionList: null,
    dataAccess: null
  })

  userInfo.role = roleObj
  return builder(userInfo)
}

const adminNav = [
  // 审核统计
  {
    name: 'reviewed-reports',
    parentId: 0,
    id: 1,
    meta: {
      icon: 'dashboard',
      title: '审核统计',
      show: true
    },
    component: 'RouteView'
  },
  {
    name: 'Analysis',
    parentId: 1,
    id: 7,
    meta: {
      title: '综合看板',
      show: true
    },
    component: 'Analysis'
  },
  {
    name: 'monitor',
    parentId: 1,
    id: 3,
    meta: {
      title: '审核日志',
      show: true
    },
    component: 'VerifyDetail'
  },
  // 审核管理
  {
    name: 'form',
    parentId: 0,
    id: 10,
    meta: {
      icon: 'form',
      title: '审核管理'
    },
    redirect: '/form/base-form',
    component: 'RouteView'
  },
  {
    name: 'basic-form',
    parentId: 10,
    id: 6,
    meta: {
      title: '工作台'
    },
    // component: 'BasicForm'
    component: 'Workplace'
  },
  {
    name: 'step-form',
    parentId: 10,
    id: 5,
    meta: {
      title: '待审核列表'
    },
    component: 'VerifyDetail'
  },
  {
    name: 'advanced-form',
    parentId: 10,
    id: 4,
    meta: {
      title: '审核快照列表'
    },
    component: 'TableList'
  }
]

const staffNav = [
  // 审核统计
  {
    name: 'reviewed-reports',
    parentId: 0,
    id: 1,
    meta: {
      icon: 'dashboard',
      title: '审核统计',
      show: true
    },
    component: 'RouteView'
  }
]

const superNav = [
  // 审核统计
  {
    name: 'reviewed-reports',
    parentId: 0,
    id: 1,
    meta: {
      icon: 'dashboard',
      title: '审核统计',
      show: true
    },
    redirect: 'reviewed-reports/reviewed-dashboard',
    component: 'RouteView'
  },
  {
     name: 'reviewed-dashboard',
    parentId: 1,
    id: 7,
    meta: {
      title: '综合看板',
      show: true
    },
    component: 'ReportDashBoard'
  },
  {
    name: 'report-logs',
    parentId: 1,
    id: 3,
    meta: {
      title: '审核日志',
      show: true
    },
    component: 'VerifyReportLogs'
  },
  // 审核管理
  {
    name: 'verify-manager',
    parentId: 0,
    id: 10,
    meta: {
      icon: 'form',
      title: '审核管理'
    },
    redirect: '/verify-manager/workplace',
    component: 'RouteView'
  },
  {
    name: 'workplace',
    parentId: 10,
    id: 6,
    meta: {
      title: '工作台'
    },
    // component: 'BasicForm'
    component: 'Workplace'
  },
  {
    name: 'untransfertask-list',
    parentId: 10,
    id: 7,
    meta: {
      title: '待转换任务'
    },
    component: 'UnTransferTaskList'
  },
  {
    id: 11,
    path: '/dashboard/VerifyDetail',
    name: 'verify-detail',
    component: 'VerifyDetail',
    hideInMenu: true
  },
  {
    name: 'undispatchtask-list',
    parentId: 10,
    id: 8,
    meta: {
      title: '待派单任务'
    },
    component: 'UnDispatchTaskList'
  },
  {
    name: 'unreviewed-list',
    parentId: 10,
    id: 5,
    meta: {
      title: '待审核任务'
    },
    component: 'UnReviewReportList'
  },
  {
    name: 'reviewed-snapshot',
    parentId: 10,
    id: 4,
    meta: {
      title: '已审核快照'
    },
    component: 'ReviewSnapshotList'
  },
  {
    name: 'reviewed-logs',
    parentId: 10,
    id: 3,
    meta: {
      title: '已审核日志'
    },
    component: 'ReviewedReportLogs'
  },

  // 个人中心
  {
    name: 'person-center',
    parentId: 0,
    id: 10010,
    meta: {
      icon: 'table',
      title: '个人中心',
      show: true
    },
    redirect: '/person-center/base-settings',
    component: 'RouteView'
  },
  {
    name: 'base-settings',
    parentId: 10010,
    id: 10011,
    meta: {
      title: '基本设置',
      show: true
    },
    component: 'BasicSetting'
  },
  {
    name: 'security-settings',
    path: '/account/settings/security',
    parentId: 10010,
    id: 10012,
    meta: {
      title: '安全设置',
      show: true
    },
    component: 'SecuritySettings'
  },
  {
    name: 'binding-settings',
    parentId: 10010,
    id: 10013,
    meta: {
      title: '账号绑定',
      show: true
    },
    path: '/account/settings/binding',
    component: 'BindingSetting'
  },
  {
    name: 'notification-settings',
    parentId: 10010,
    id: 10014,
    meta: {
      title: '新消息通知',
      show: true
    },
    path: '/account/settings/notification',
    component: 'NotificationSettings'
  },
  // 系统设置
  {
    name: 'system',
    parentId: 0,
    id: 20010,
    meta: {
      icon: 'setting',
      title: '系统设置',
      show: true
    },
    redirect: '/system/globle-setting',
    component: 'RouteView'
  },
  {
    name: 'globle-setting',
    parentId: 20010,
    id: 20011,
    meta: {
      title: '全局变量设置',
      show: true
    },
    path: '/system/globle-setting',
    component: 'GlobleSetting'
  },
  {
    name: 'role-setting',
    parentId: 20010,
    id: 20012,
    meta: {
      title: '账号角色设置',
      show: true
    },
    path: '/system/role-setting',
    component: 'RoleSetting'
  },
  {
    name: 'user-permissionsetting',
    parentId: 20010,
    id: 20013,
    meta: {
      title: '账号权限设置',
      show: true
    },
    path: '/system/user-permissionsetting',
    component: 'UserPermissionSetting'
  },
  {
    name: 'user-setting',
    parentId: 20010,
    id: 20014,
    meta: {
      title: '账号设置',
      show: true
    },
    path: '/system/user-setting',
    component: 'UserSetting'
  }
]

const userNav = options => {
  let nav
  if (options.body === '456') {
    nav = adminNav
  } else if (options.body === '123') {
    nav = staffNav
  } else if (options.body === '789') {
    nav = superNav
  }
  nav = superNav

  const json = builder(nav)
  console.log('json', json)
  return json
}

Mock.mock(/\/api\/user\/info/, 'get', info)
Mock.mock(/\/api\/user\/nav/, 'post', userNav)
