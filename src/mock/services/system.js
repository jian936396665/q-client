import Mock from 'mockjs2'
import { builder, getQueryParameters } from '../util'
const totalCount = 5701

// const getSystemRoleSettingList = (options) => {
//   const parameters = getQueryParameters(options)

//   const result = []
//   const pageNo = parseInt(parameters.pageNo)
//   const pageSize = parseInt(parameters.pageSize)
//   const totalPage = Math.ceil(totalCount / pageSize)
//   const key = (pageNo - 1) * pageSize
//   const next = (pageNo >= totalPage ? (totalCount % pageSize) : pageSize) + 1

//   for (let i = 1; i < next; i++) {
//     const tmpKey = key + i
//     result.push({
//       key: tmpKey,
//       id: tmpKey,
//       no: 'No ' + tmpKey,
//       description: '明朝那些事儿',
//       callNo: Mock.mock('@integer(1, 999)'),
//       status: Mock.mock('@integer(0, 3)'),
//       updatedAt: Mock.mock('@datetime'),
//       editable: false,
//       duration: '00:12:00'
//     })
//   }

//   return builder({
//     pageSize: pageSize,
//     pageNo: pageNo,
//     totalCount: totalCount,
//     totalPage: totalPage,
//     data: result
//   })
//   }

const freeDispatchList = (options) => {
  const parameters = getQueryParameters(options)
  const result = []
  const pageNo = parseInt(parameters.pageNo)
  const pageSize = parseInt(parameters.pageSize)
  const totalPage = Math.ceil(totalCount / pageSize)
  const key = (pageNo - 1) * pageSize
  const next = (pageNo >= totalPage ? (totalCount % pageSize) : pageSize) + 1

  for (let i = 1; i < next; i++) {
    const tmpKey = key + i
    result.push({
      // key: tmpKey,
      id: tmpKey,
      no: tmpKey,
      // changedLineNum: '10',
      // totalLineNum: '2',
      // // 音频名称
      // audioName: Mock.mock('@cparagraph()'),
      // description: Mock.mock('@cparagraph()'),
      // // 派单对象
      // dispatchUser: Mock.mock('@cname'),
      // callNo: Mock.mock('@integer(1, 999)'),
      // status: Mock.mock('@integer(0, 3)'),
      // // 审核状态
      // reviewStatus: Mock.mock('@integer(0, 3)'),
      // queueStatus: Mock.mock('@integer(0, 3)'),
      // updatedAt: Mock.mock('@datetime'),
      // editable: false,
      // editor: '小酷',
      // audioDuration: '00:11:54',
      // // 派单时间
      // dispatchTime: Mock.mock('@datetime'),
      // // 审核时间
      // reviewTime: Mock.mock('@datetime'),
      // uploadTime: Mock.mock('@datetime'),
      // editType: '提交 批量暂存文本 19条（1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19）',
      userId: Mock.mock('@integer(1,100)'),
      nickName: Mock.mock('@cname'),
      lastLoginTime: Mock.mock('@datetime'),
      lastReviewTime: Mock.mock('@datetime'),
      loginStatus: Mock.mock('@integer(1,3)'),
      currentTaskCount: Mock.mock('@integer(1,100)'),
      finishTaskCount: Mock.mock('@integer(1,100)'),
      point: Mock.mock('@integer(1,100)')
    })
  }

  return builder({
    pageSize: pageSize,
    pageNo: pageNo,
    totalCount: totalCount,
    totalPage: totalPage,
    data: result
  })
}

Mock.mock(/\/system\/role-setting-list/, 'get', freeDispatchList)
