import Mock from 'mockjs2'
import { builder, getQueryParameters } from '../util'
const totalCount = 5701

const freeDispatchList = (options) => {
    const parameters = getQueryParameters(options)
    const result = []
    const pageNo = parseInt(parameters.pageNo)
    const pageSize = parseInt(parameters.pageSize)
    const totalPage = Math.ceil(totalCount / pageSize)
    const key = (pageNo - 1) * pageSize
    const next = (pageNo >= totalPage ? (totalCount % pageSize) : pageSize) + 1

    for (let i = 1; i < next; i++) {
      const tmpKey = key + i
      result.push({
        // key: tmpKey,
        id: tmpKey,
        no: tmpKey,
        // changedLineNum: '10',
        // totalLineNum: '2',
        // // 音频名称
        // audioName: Mock.mock('@cparagraph()'),
        // description: Mock.mock('@cparagraph()'),
        // // 派单对象
        // dispatchUser: Mock.mock('@cname'),
        // callNo: Mock.mock('@integer(1, 999)'),
        // status: Mock.mock('@integer(0, 3)'),
        // // 审核状态
        // reviewStatus: Mock.mock('@integer(0, 3)'),
        // queueStatus: Mock.mock('@integer(0, 3)'),
        // updatedAt: Mock.mock('@datetime'),
        // editable: false,
        // editor: '小酷',
        // audioDuration: '00:11:54',
        // // 派单时间
        // dispatchTime: Mock.mock('@datetime'),
        // // 审核时间
        // reviewTime: Mock.mock('@datetime'),
        // uploadTime: Mock.mock('@datetime'),
        // editType: '提交 批量暂存文本 19条（1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19）',
        userId: Mock.mock('@integer(1,100)'),
        nickName: Mock.mock('@cname'),
        lastLoginTime: Mock.mock('@datetime'),
        lastReviewTime: Mock.mock('@datetime'),
        loginStatus: Mock.mock('@integer(1,3)'),
        currentTaskCount: Mock.mock('@integer(1,100)'),
        finishTaskCount: Mock.mock('@integer(1,100)'),
        point: Mock.mock('@integer(1,100)')
      })
    }

    return builder({
      pageSize: pageSize,
      pageNo: pageNo,
      totalCount: totalCount,
      totalPage: totalPage,
      data: result
    })
  }

/**
 * 转换详情
 */
const transferDetail = () => {
    return builder({
        'data': {
            'audioId': '61500e88a7e912474847ddb8',
            'audioName': 'ccccccccccccc',
            // 'audioUri': 'http://8.140.173.110:8000/api/aiDownload/585081290244489216/585082667347738624.mp3',
            'andioUri': 'http://f.ztj.xyz/t/test.m4a',
            'audioCreatedAt': 1632636552,
            'translateJoinAt': 1632653737,
            'textCount': 1234,
            'textNum': 12345678,
            'translateTaskId': '7ec243b7affe46db85646087139d7e8d',
            'audioLength': 1141263,
            'translateResult': [
                {
                    'sentences': '世上生意甚多，',
                    'starttime': 4240,
                    'endtime': 6100,
                    'duration': 2111
                },
                {
                    'sentences': '惟有说书难习。',
                    'starttime': 7360,
                    'endtime': 9480,
                    'duration': 2111
                },
                {
                    'sentences': '世上生意甚多，',
                    'starttime': 4240,
                    'endtime': 6100,
                    'duration': 2111
                },
                {
                    'sentences': '惟有说书难习。',
                    'starttime': 7360,
                    'endtime': 9480,
                    'duration': 2111
                },
                {
                    'sentences': '世上生意甚多，',
                    'starttime': 4240,
                    'endtime': 6100,
                    'duration': 2111
                },
                {
                    'sentences': '惟有说书难习。',
                    'starttime': 7360,
                    'endtime': 9480,
                    'duration': 2111
                }
            ],
            'translateState': 2,
            'translateCompletedAt': 1632711373
        }
    })
}

/**
 * 转换详情文本列表
 */
const convertTranslateResultList = () => {
    return builder({
        'data': [
            {
                'sentences': '世上生意甚多，',
                'starttime': 4240,
                'endtime': 6100,
                'duration': 2,
                'audioLengthDes': '00:00:02'
            },
            {
                'sentences': '惟有说书难习。',
                'starttime': 7360,
                'endtime': 9480,
                'duration': 4,
                'audioLengthDes': '00:00:04'
            },
            {
                'sentences': '世上生意甚多，',
                'starttime': 4240,
                'endtime': 6100,
                'duration': 6,
                'audioLengthDes': '00:00:06'
            },
            {
                'sentences': '惟有说书难习。',
                'starttime': 7360,
                'endtime': 9480,
                'duration': 8,
                'audioLengthDes': '00:00:08'
            },
            {
                'sentences': '世上生意甚多，',
                'starttime': 4240,
                'endtime': 6100,
                'duration': 10,
                'audioLengthDes': '00:00:10'
            },
            {
                'sentences': '惟有说书难习。',
                'starttime': 7360,
                'endtime': 9480,
                'duration': 12,
                'audioLengthDes': '00:00:12'
            }
        ]
    })
}

const snopshotDetail = () => {
    return builder({
        'data': {
            snopshotInfo: {
                snapshotId: 12,
                time: '00:12:45',
                exchangeText: 34,
                audioName: '解忧杂货店',
                duration: '00:12:45',
                textCount: '123'
            },
            textList: [
                {
                    duration: '00:10:00',
                    text: '原文本行：已经被删除的文本加粗标红；快照文本行：新增的文本',
                    status: 0
                },
                {
                    duration: '00:10:00',
                    text: '对快照中的文本行，也可以进行再次编辑，但不能进行单行提交，只能对单行修改进行暂存，并批量提交',
                    status: 1
                },
                {
                    duration: '00:10:00',
                    text: '原文本行：已经被删除的文本加粗标红；快照文本行：新增的文本',
                    status: 0
                },
                {
                    duration: '00:10:00',
                    text: '对快照中的文本行，也可以进行再次编辑，但不能进行单行提交，只能对单行修改进行暂存，并批量提交',
                    status: 1
                },
                {
                    duration: '00:10:00',
                    text: '原文本行：已经被删除的文本加粗标红；快照文本行：新增的文本',
                    status: 0
                },
                {
                    duration: '00:10:00',
                    text: '对快照中的文本行，也可以进行再次编辑，但不能进行单行提交，只能对单行修改进行暂存，并批量提交',
                    status: 1
                }
            ]
        }
    })
}

Mock.mock(/\/verify\/snapshotDetail/, 'get', snopshotDetail)
Mock.mock(/\/verify\/transferDetail/, 'get', transferDetail)
Mock.mock(/\/free-dispatch\/list/, 'get', freeDispatchList)
Mock.mock(/\/verify\/convertTranslateResultList/, 'get', convertTranslateResultList)
