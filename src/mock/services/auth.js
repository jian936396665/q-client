import Mock from 'mockjs2'
import { builder, getBody } from '../util'

const username = ['staff', 'admin', 'super']

const password = ['1253208465b1efa876f982d8a9e73eef', '21232f297a57a5a743894a0e4a801fc3', '1b3231655cebb7a1f783eddf27d254ca']

const login = (options) => {
  const body = getBody(options)
  console.log('mock: body', body)
  if (!username.includes(body.username) || !password.includes(body.password)) {
    return builder({ isLogin: true }, '账户或密码错误', 401)
  }

  let roleType = body.username
  let mtoken
  if (body.username.includes('staff')) {
    roleType = 'staff'
    mtoken = '123'
  }

  if (body.username.includes('admin')) {
    roleType = 'admin'
    mtoken = '456'
  }

  if (body.username.includes('super')) {
    roleType = 'super'
    mtoken = '789'
  }

  return builder({
    'id': Mock.mock('@guid'),
    'name': Mock.mock('@name'),
    'username': 'admin',
    'password': '',
    'avatar': 'https://gw.alipayobjects.com/zos/rmsportal/jZUIxmJycoymBprLOUbT.png',
    'status': 1,
    'telephone': '',
    'lastLoginIp': '27.154.74.117',
    'lastLoginTime': 1534837621348,
    'creatorId': 'admin',
    'createTime': 1497160610259,
    'deleted': 0,
    'roleId': roleType,
    'lang': 'zh-CN',
    'token': mtoken
  }, '', 200, { 'Custom-Header': Mock.mock('@guid') })
}

const logout = () => {
  return builder({}, '[测试接口] 注销成功')
}

const smsCaptcha = () => {
  return builder({ captcha: Mock.mock('@integer(10000, 99999)') })
}

const twofactor = () => {
  return builder({ stepCode: Mock.mock('@integer(0, 1)') })
}

Mock.mock(/\/auth\/login/, 'post', login)
Mock.mock(/\/auth\/logout/, 'post', logout)
Mock.mock(/\/account\/sms/, 'post', smsCaptcha)
Mock.mock(/\/auth\/2step-code/, 'post', twofactor)
