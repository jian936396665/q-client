import request from '@/utils/request'

const authVersion = '/auth/v1'
const userVersion = '/user/v1'

const userApi = {
  Login: '/auth/v1/login',
  Logout: '/auth/logout',
  ForgePassword: '/auth/forge-password',
  Register: '/auth/v1/register',
  twoStepCode: '/auth/2step-code',
  SendSms: '/account/sms',
  SendSmsErr: '/account/sms_err',
  // get my info
  UserInfo: '/user/v1/userInfo',
  UserMenu: '/user/v1/userNav',
  // 通用发送验证码
  SmsCode: authVersion + '/smsCode',
  // 发送邮箱验证码
  emailCode: authVersion + '/emailCode',
  // 更新用户信息
  updateUserInfo: userVersion + '/updateUserInfo',
  // 忘记密码，验证信息
  verifyUserInfo: authVersion + '/verifyUserInfo',
  // 修改密码
  resetPassWord: authVersion + '/resetPassWord',
  // 用户列表
  userList: userVersion + '/userList'
}

/**
 * login func
 * parameter: {
 *     username: '',
 *     password: '',
 *     remember_me: true,
 *     captcha: '12345'
 * }
 * @param parameter
 * @returns {*}
 */
 const devBaseUrl = 'http://8.140.173.177:30080'
// const localhost = 'http://localhost:8090'

export function verifyUserInfo (parameter) {
  return request({
    baseURL: devBaseUrl,
    url: userApi.verifyUserInfo,
    method: 'post',
    data: parameter
  })
}

export function resetPassWord (parameter) {
  return request({
    baseURL: devBaseUrl,
    url: userApi.resetPassWord,
    method: 'post',
    data: parameter
  })
}

export function SmsCode (parameter) {
  return request({
    baseURL: devBaseUrl,
    url: userApi.SmsCode,
    method: 'get',
    params: parameter
  })
}

export function userList (parameter) {
  return request({
    baseURL: devBaseUrl,
    url: userApi.userList,
    method: 'get',
    params: parameter
  })
}

export function emailCode (parameter) {
  return request({
    baseURL: devBaseUrl,
    url: userApi.emailCode,
    method: 'get',
    params: parameter
  })
}

export function updateUserInfo (parameter) {
  return request({
    baseURL: devBaseUrl,
    url: userApi.updateUserInfo,
    method: 'post',
    data: parameter
  })
}

export function login (parameter) {
  return request({
    baseURL: devBaseUrl,
    url: userApi.Login,
    method: 'post',
    data: parameter
  })
}

export function register (parameter) {
  return request({
    baseURL: devBaseUrl,
    url: userApi.Register,
    method: 'post',
    data: parameter
  })
}

export function getSmsCaptcha (parameter) {
  return request({
    url: userApi.SendSms,
    method: 'post',
    data: parameter
  })
}

export function getInfo (parameter) {
  return request({
    baseURL: devBaseUrl,
    url: userApi.UserInfo,
    method: 'get',
    params: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

export function getCurrentUserNav (parameter) {
  return request({
    baseURL: devBaseUrl,
    url: userApi.UserMenu,
    method: 'get',
    params: parameter
  })
}

export function logout () {
  return request({
    url: userApi.Logout,
    method: 'post',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

/**
 * get user 2step code open?
 * @param parameter {*}
 */
export function get2step (parameter) {
  return request({
    url: userApi.twoStepCode,
    method: 'post',
    data: parameter
  })
}
