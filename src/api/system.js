import request from '@/utils/request'

const userVersion = '/user/v1'
const devBaseUrl = 'http://8.140.173.177:30080'
// const localhost = 'http://localhost:8090'
const systemApi = {
    // 创建角色
    createRole: userVersion + '/userRole/createRole',
    // 账号角色设置列表
    UserRoleList: userVersion + '/userRole/roleList',
    // 账号设置列表
    userList: userVersion + '/userList'
}

export function userList (parameter) {
  return request({
    baseURL: devBaseUrl,
    url: systemApi.userList,
    method: 'get',
    params: parameter
  })
}

export function createRole (parameter) {
  return request({
    baseURL: devBaseUrl,
    url: systemApi.createRole,
    method: 'post',
    data: parameter
  })
}

export function UserRoleList (parameter) {
  return request({
    baseURL: devBaseUrl,
    url: systemApi.UserRoleList,
    method: 'get',
    params: parameter
  })
}
