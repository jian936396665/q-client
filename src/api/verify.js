import request from '@/utils/request'

const audioVersion = '/audio/v1'
const dispatchVersion = '/dispatch/v1'
const reviewVersion = '/review/v1'
const devBaseUrl = 'http://8.140.173.177:30080'
// const localhost = 'http://localhost:8090'

const verifyApi = {
    VerifiyInfo: '/verify/awaitList',
    SnapshotDetail: '/verify/snapshotDetail',
    freeDispatchList: '/free-dispatch/list',
    // 待转换详情（音频详情也也可以）
    convertDetail: '/audio/v1/convertDetail',
    // 待转换详情批量提交
    convertSubmitSnapShot: '/audio/v1/convertSubmitSnapShot',
    // 创建任务
    createConvert: audioVersion + '/createConvert',
    // 待转换任务列表
    convertList: audioVersion + '/convertList',
    // 待审核任务列表
    waitReview: reviewVersion + '/waitReview',
    // 待派单任务列表
    waitList: dispatchVersion + '/waitList',
    // 已审核快照列表
    convertSnapshotList: audioVersion + '/convertSnapshotList',
    // 快照详情
    convertSnapshotDetail: audioVersion + '/convertSnapshotDetail',
    // 指派任务
    addTask: dispatchVersion + '/addTask',
    // 提交快照生成快照日志
    SubmitSnapShot: reviewVersion + '/SubmitSnapShot',
    convertTotalTime: audioVersion + '/convertTotalTime'
}

export function verifyInfo (parameter) {
  return request({
    url: verifyApi.VerifiyInfo,
    method: 'get',
    params: parameter
  })
}

export function convertTotalTime (parameter) {
  return request({
    baseURL: devBaseUrl,
    url: verifyApi.convertTotalTime,
    method: 'get',
    params: parameter
  })
}

export function getFreeDispatchList (parameter) {
  return request({
    url: verifyApi.freeDispatchList,
    method: 'get',
    params: parameter
  })
}

/**
 * 待审核详情
 * @param {} parameter
 */
export function convertDetail (parameter) {
  return request({
    baseURL: devBaseUrl,
    url: verifyApi.convertDetail,
    method: 'get',
    params: parameter
  })
}

/**
 * 提交批量暂存生成快照
 * @param {*} parameter
 */
export function convertSubmitSnapShot (parameter) {
  return request({
    baseURL: devBaseUrl,
    url: verifyApi.convertSubmitSnapShot,
    method: 'post',
    data: parameter
  })
}

/**
 * 二次提交快照生成日志
 * @param {*} parameter
 */
export function SubmitSnapShot (parameter) {
  return request({
    baseURL: devBaseUrl,
    url: verifyApi.SubmitSnapShot,
    method: 'post',
    data: parameter
  })
}

/**
 * 创建任务
 * @param {*} parameter
 */
export function createConvert (parameter) {
  return request({
    baseURL: devBaseUrl,
    url: verifyApi.createConvert,
    method: 'post',
    data: parameter
  })
}

/**
 * 待转换任务列表
 * @param {*} parameter
 */
export function convertList (parameter) {
  return request({
    baseURL: devBaseUrl,
    url: verifyApi.convertList,
    method: 'get',
    params: parameter
  })
}

/**
 * 待派单任务列表
 * @param {*} parameter
 */
export function waitList (parameter) {
  return request({
    baseURL: devBaseUrl,
    url: verifyApi.waitList,
    method: 'get',
    params: parameter
  })
}

/**
 * 待审核任务列表
 * @param {*} parameter
 */
export function waitReview (parameter) {
  return request({
    baseURL: devBaseUrl,
    url: verifyApi.waitReview,
    method: 'get',
    params: parameter
  })
}

/**
 * 已审核快照
 * @param {*} parameter
 */
export function convertSnapshotList (parameter) {
  return request({
    baseURL: devBaseUrl,
    url: verifyApi.convertSnapshotList,
    method: 'get',
    params: parameter
  })
}

/**
 * 快照详情
 * @param {*} parameter
 */
export function convertSnapshotDetail (parameter) {
  return request({
    baseURL: devBaseUrl,
    url: verifyApi.convertSnapshotDetail,
    method: 'get',
    params: parameter
  })
}

/**
 * 指派任务
 * @param {*} parameter
 */
export function addTask (parameter) {
  return request({
    baseURL: devBaseUrl,
    url: verifyApi.addTask,
    method: 'post',
    data: parameter
  })
}
