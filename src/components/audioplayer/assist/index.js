const util = {}

/**
 * 格式化分秒
 * @param  {[type]} value [description]
 * @return {[type]}       [description]
 */
util.timeFormat = function (value) {
	if (value === 0) return '00:00'
	let minutes = parseInt(value / 60)
    let seconds = parseInt(value % 60)
	minutes = minutes < 10 ? ('0' + minutes) : minutes
    seconds = seconds < 10 ? ('0' + seconds) : seconds
    return minutes + ':' + seconds
}

/**
 * 毫秒转化时：分：秒
 */
util.formatHMS = function (value) {
    value = parseInt(value / 1000)
    if (value >= 60 && value <= 3600) {
        value = '00:' + parseInt(value / 60) + ':' + (value % 60 < 10 ? '0' + value % 60 : value % 60)
    } else {
        if (value > 3600) {
            value = parseInt(value / 3600) + ':' + parseInt(((value % 3600) / 60)) + ':' + (value % 60 < 10 ? value % 60 + '0' : value % 60)
        } else {
            value = '00:00:' + (value < 10 ? '0' + value : value)
        }
    }
    return value
}

/**
* 格式化百分比
* @param  {[type]} value [description]
* @return {[type]}       [description]
*/
util.percentFormat = function (value) {
	return (value * 100)
}

/**
 * 获取元素相对body位置
 * @param  {[type]} element [description]
 * @return {[type]}         [description]
 */
util.getOffset = function (element) {
	let top = element.offsetTop
	let left = element.offsetLeft
    while (element.offsetParent) {
        element = element.offsetParent
        top += element.offsetTop
        left += element.offsetLeft
    }
    const scrollTop = document.body.scrollTop + document.documentElement.scrollTop
    const scrollLeft = document.body.scrollLeft + document.documentElement.scrollLeft
    return { top: top - scrollTop, left: left - scrollLeft }
}

export default util
