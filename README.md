# 腾讯音转文平台

## 架构

Ant Design Pro，Vue，Vuex

## 项目概述

音频音转文，内容审核平台，二次审核提交日志。[依图长语音转译]](https://speech.yitutech.com/intro/longaudio)

## 使用

npm install / cnpm install

npm run serve